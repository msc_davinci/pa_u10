package org.davinci.aspectouno;

import javax.swing.JFrame;

public class EventMonitor extends JFrame {
    public static void main(String[] args) {
        EventMonitorFrame application = new EventMonitorFrame();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.setSize(480, 300); // set size of window
        application.setVisible(true); // show window
    }
}
