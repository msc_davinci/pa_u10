package org.davinci.aspectouno;

import java.awt.BorderLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class EventMonitorFrame extends JFrame {
    private JComboBox eventsComboBox;
    private JButton okButton;
    private JTextArea outputTextArea;
    private JPanel controlPanel;
    private String eventSelected = "ActionEvent";
    private String events[] =
            {"ActionEvent", "ItemEvent", "KeyEvent", "MouseEvent"};

    // set up GUI
    public EventMonitorFrame() {
        super("Monitoring Events");
        setLayout(new BorderLayout()); // set frame layout

        // set up JComboBox and register its event handler
        eventsComboBox = new JComboBox(events);
        eventsComboBox.setMaximumRowCount(events.length);
        eventsComboBox.addItemListener(new ComboBoxHandler());
        eventsComboBox.addKeyListener(new KeyHandler());

        // set up OK button and register its event handler
        okButton = new JButton("OK");
        okButton.addActionListener(new ActionHandler());
        okButton.addKeyListener(new KeyHandler());

        // textarea to display events
        outputTextArea = new JTextArea(8, 50);
        outputTextArea.setLineWrap(true);
        outputTextArea.setWrapStyleWord(true);

        // register mouse, key event handlers
        addMouseListener(new MouseHandler());

        controlPanel = new JPanel();
        controlPanel.add(eventsComboBox);
        controlPanel.add(okButton);
        add(controlPanel, BorderLayout.NORTH);
        add(new JScrollPane(outputTextArea), BorderLayout.CENTER);
    } // end EventMonitorFrame constructor

    // class handles combo box event
    private class ComboBoxHandler implements ItemListener {
        public void itemStateChanged(ItemEvent event) {
            eventSelected = (String) eventsComboBox.getSelectedItem();
            if (eventSelected.equals("ItemEvent"))
                outputTextArea.append(String.format(
                        "%s\n", event.toString()));
        } // end method itemStateChanged
    } // end inner class ComboBoxHandler

    // class handles mouse event
    private class MouseHandler extends MouseAdapter {
        public void mouseClicked(MouseEvent event) {
            eventSelected = (String) eventsComboBox.getSelectedItem();
            if (eventSelected.equals("MouseEvent"))
                outputTextArea.append(String.format(
                        "%s\n", event.toString()));
        } // end method mouseClicked
    } // end inner class MouseHandler

    // class handles action event
    private class ActionHandler implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            eventSelected = (String) eventsComboBox.getSelectedItem();
            if (eventSelected.equals("ActionEvent"))
                outputTextArea.append(String.format(
                        "%s\n", event.toString()));
        } // end method actionPerformed
    } // end inner class ActionHandler

    // class handles key event
    private class KeyHandler extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            eventSelected = (String) eventsComboBox.getSelectedItem();
            if (eventSelected.equals("KeyEvent"))
                outputTextArea.append(String.format(
                        "%s\n", event.toString()));
        } // end method keyTyped
    } // end inner class KeyHandler
}
