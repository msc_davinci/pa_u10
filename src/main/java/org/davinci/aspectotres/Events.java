package org.davinci.aspectotres;

import javax.swing.*;

public class Events {
    public static void main(String args[]) {
        EventsFrame eventsFrame = new EventsFrame(); // create EventsFrame
        eventsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        eventsFrame.setSize(375, 325); // set frame size
        eventsFrame.setVisible(true); // display frame
    }
}
